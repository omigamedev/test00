#include <engine/engine.h>
#include <engine/engine_gui.h>
#include <engine/engine_scene.h>
#include <stdarg.h>
#include <math.h>

engine_video_t *g_video = NULL;

void app_main()
{
    LOG("main");
}
void app_init(engine_video_t *video)
{
    g_video = video;
    float ratio = (float)video->screen_width / (float)video->screen_height;
    float hh = 1.0f;
    float ww = hh * ratio;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrthof(-ww, ww, -hh, hh, -10.0f, 100.0f);
    glViewport(0, 0, video->screen_width, video->screen_height);
    glMatrixMode(GL_MODELVIEW);

    for(int i = 0; i < 0xFFFF; i++) glDisable(i);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_BLEND);

    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
}
void app_draw(int ms)
{
}
void app_pause()
{
}
void app_resume()
{
}
void app_destroy()
{
}
void app_touch_down(int x, int y, int tid)
{
}
void app_touch_move(int x, int y, int tid)
{
}
void app_touch_up(int x, int y, int tid)
{
}
void app_accelerate(float x, float y, float z)
{
}
int app_key_down(int code)
{
    return 0;
}
int app_key_up(int code)
{
    return 0;
}
